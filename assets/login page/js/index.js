$().ready(function () {
    $("#login-button").on("click", function (event) {
        event.preventDefault();

        $('form').fadeOut(500);
        $('.wrapper').addClass('form-success');

    });

    $("#pswrd_show").on("click", function () {

        if ($(this).hasClass("fas fa-eye")) {
            $(this).removeClass("fas fa-eye");
            $(this).addClass("fas fa-eye-slash");
            $('#pswrd').attr('type', 'Password');
        } else if ($(this).hasClass("fas fa-eye-slash")) {
            $(this).removeClass("fas fa-eye-slash");
            $(this).addClass("fas fa-eye");
            $('#pswrd').attr('type', 'Text');

        }

    });

});